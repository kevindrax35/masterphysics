import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Side from "./src/components/Side";
import Router from "./src/router/Router";
// import Rol from "../screens/Rol";

export default function App() {
  return <Router />;
}
