import React, { useState } from "react";
import { View } from "react-native";
import {
  Image,
  StyleSheet,
  ActivityIndicator,
  TouchableOpacity,
  Text,
} from "react-native";
import { useFonts } from "expo-font";
import Avatar from "../../assets/images/avatar.png";
import { List } from "react-native-paper";
import logout from "../../assets/images/logout.png";
import CloseMenu from "../../assets/images/closemenu.png";

export default function Side({ navigation, route }) {
  const [sideMenuStatus, setSideMenuStatus] = useState(false);
  const [loaded] = useFonts({
    Montserrat: require("../../assets/fonts/Montserrat.ttf"),
    MontserratBold: require("../../assets/fonts/Montserrat-Bold.ttf"),
  });

  const sideMenuHandler = () => {
    setSideMenuStatus(!sideMenuStatus);
  };

  if (!loaded) {
    return <ActivityIndicator color="#4d56" animating={true} />;
  }
  return (
    <View style={styles.container}>
      <View style={sideMenuStatus ? styles.sideMenu : styles.sideClose}>
        <TouchableOpacity onPress={sideMenuHandler} style={styles.btnMenu}>
          <Image
            source={CloseMenu}
            style={{
              width: 30,
              height: 30,
              marginTop: 10,
              marginLeft: 10,
            }}
          ></Image>
        </TouchableOpacity>
        <View style={styles.header}>
          <Image
            source={Avatar}
            style={{ width: 90, height: 90, marginBottom: 10 }}
          />
          <Text style={styles.title}>Usuario #31000</Text>
          <View style={styles.list}>
            <List.Item
              titleStyle={{ color: "#fff", fontFamily: "Montserrat" }}
              title="Inicio"
            />
            <List.Item
              titleStyle={{ color: "#fff", fontFamily: "Montserrat" }}
              title="Tematicas"
            />
            <List.Item
              titleStyle={{ color: "#fff", fontFamily: "Montserrat" }}
              title="Aula virtual"
            />
            <List.Item
              titleStyle={{ color: "#fff", fontFamily: "Montserrat" }}
              title="Recursos"
            />
            <List.Item
              titleStyle={{ color: "#fff", fontFamily: "Montserrat" }}
              title="Cuenta"
            />
            <List.Item
              titleStyle={{ color: "#fff", fontFamily: "Montserrat" }}
              title="Configuración"
            />
            <View style={styles.logout}>
              <Image source={logout} style={{ width: 30, height: 30 }} />
              <Text
                onPress={() => navigation.navigate("Login")}
                style={{
                  color: "#fff",
                  fontSize: 15,
                  fontFamily: "MontserratBold",
                  marginLeft: 10,
                }}
              >
                Cerrar sesión
              </Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#000",
    height: "100%",
    width: "100%",
  },

  btnMenu: {
    backgroundColor: "#2FEFD5",
    width: 50,
    height: 50,
    marginTop: 20,
    marginLeft: 20,
    borderRadius: 30,
  },

  header: {
    flex: 1,
    alignItems: "center",
  },

  sideMenu: {
    flex: 1,
    width: 300,
    backgroundColor: "#353535",
    borderTopRightRadius: 30,
  },

  title: {
    color: "#fff",
    fontSize: 16,
    fontFamily: "MontserratBold",
  },

  list: {
    marginTop: 30,
    width: 280,
    color: "#fff",
    backgroundColor: "#353535",
    fontFamily: "Montserrat",
  },

  logout: {
    flex: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 100,
  },

  sideClose: {
    overflow: "hidden",
  },
});
