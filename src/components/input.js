import React, { Component } from "react";
import { TextInput, View } from "react-native";

export default function UselessTextInput() {
  return (
    <View>
      <TextInput
        style={{ height: 40, borderColor: "gray", borderWidth: 1 }}
        onChangeText={(text) => onChangeText(text)}
        placeholder="hola"
      />
      <TextInput
        style={{ height: 40, borderColor: "gray", borderWidth: 1 }}
        onChangeText={(text) => onChangeText(text)}
      />
    </View>
  );
}
