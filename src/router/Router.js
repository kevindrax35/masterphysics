import * as React from "react";
import { View, Text } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import Login from "../screens/Login";
import Rol from "../screens/Rol";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import StudentHome from "../screens/StudentHome";

const Stack = createNativeStackNavigator();

function Router() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Login"
        screenOptions={{
          headerStyle: { backgroundColor: "#5B5A5A" },
          headerTitleStyle: { fontFamily: "MontserratBold", color: "#fff" },
          headerTransparent: "false",
        }}
      >
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Rol" component={Rol} />
        <Stack.Screen name="Estudiante" component={StudentHome} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Router;
