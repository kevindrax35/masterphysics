import React from "react";
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ActivityIndicator,
} from "react-native";
import Checkbox from "expo-checkbox";
import logo from "../../assets/images/grayatom.png";
import { StatusBar } from "expo-status-bar";
import { useFonts } from "expo-font";
import { TextInput } from "react-native";

export default function Login({ navigation, route }) {
  const [loaded] = useFonts({
    Montserrat: require("../../assets/fonts/Montserrat.ttf"),
    MontserratBold: require("../../assets/fonts/Montserrat-Bold.ttf"),
  });

  if (!loaded) {
    return <ActivityIndicator color="#4d56" animating={true} />;
  }

  return (
    <View style={styles.container}>
      <View style={styles.containBox}>
        <View style={styles.containLogo}>
          <Image
            source={logo}
            style={{ width: 90, height: 90, marginBottom: 10 }}
          />
          <Text
            style={{
              color: "#fff",
              fontSize: 20,
              fontFamily: "MontserratBold",
            }}
          >
            MasterPhysics 1.0
          </Text>
        </View>

        <View style={{ marginTop: 40 }}>
          <TextInput
            style={{
              backgroundColor: "#5B5A5A",
              borderColor: "gray",
              borderRadius: 30,
              color: "#fff",
              fontFamily: "Montserrat",
              width: 260,
              height: 50,
            }}
            onChangeText={(text) => onChangeText(text)}
            placeholder="Usuario"
          />
          <TextInput
            style={{
              backgroundColor: "#5B5A5A",
              borderColor: "gray",
              borderRadius: 30,
              color: "#fff",
              fontFamily: "Montserrat",
              width: 260,
              height: 50,
              marginTop: 20,
            }}
            onChangeText={(text) => onChangeText(text)}
            placeholder="Contraseña"
          />
        </View>

        <View style={styles.checkSection}>
          <Checkbox style={styles.checkbox} color="#2FEFD5" />
          <Text style={styles.paragraph}>Recordar usuario</Text>
        </View>

        <TouchableOpacity
          onPress={() => navigation.navigate("Rol")}
          style={styles.BtnLogin}
        >
          <Text
            style={{
              fontSize: 16,
              fontFamily: "MontserratBold",
              color: "#fff",
              textAlign: "center",
              marginTop: 12,
            }}
          >
            Ingresar
          </Text>
        </TouchableOpacity>
        <StatusBar style="auto" />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#191A1A",
    alignItems: "center",
    justifyContent: "center",
  },

  containBox: {
    flex: 0,
    alignItems: "center",
    backgroundColor: "#353535",
    borderRadius: 30,
    width: 330,
    height: 500,
  },

  BtnLogin: {
    backgroundColor: "#2FEFD5",
    width: 170,
    height: 50,
    marginBottom: 30,
    borderRadius: 30,
  },

  containLogo: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 30,
  },

  checkSection: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 30,
  },

  checkbox: {},

  paragraph: {
    color: "#fff",
    fontFamily: "Montserrat",
  },
});
