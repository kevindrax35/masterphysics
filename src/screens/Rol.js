import React from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ActivityIndicator,
  Image,
} from "react-native";
import { StatusBar } from "expo-status-bar";
import { useFonts } from "expo-font";

export default function Login({ navigation, route }) {
  const [loaded] = useFonts({
    Montserrat: require("../../assets/fonts/Montserrat.ttf"),
    MontserratBold: require("../../assets/fonts/Montserrat-Bold.ttf"),
  });

  if (!loaded) {
    return <ActivityIndicator color="#4d56" animating={true} />;
  }

  return (
    <View style={styles.container}>
      <View style={styles.containBox}>
        <View style={styles.containTitle}>
          <Text
            style={{
              color: "#fff",
              fontSize: 20,
              fontFamily: "MontserratBold",
            }}
          >
            Por favor elija su rol
          </Text>
        </View>
        <View style={{ flex: 4, flexDirection: "column" }}>
          <TouchableOpacity
            activeOpacity={0.5}
            onPress={() => navigation.navigate("Estudiante")}
            style={styles.btnRol}
          >
            <Text
              style={{
                fontSize: 16,
                fontFamily: "Montserrat",
                color: "#fff",
                textAlign: "center",
              }}
            >
              Estudiante
            </Text>
          </TouchableOpacity>

          <TouchableOpacity activeOpacity={0.5} style={styles.btnRol}>
            <Text
              style={{
                fontSize: 16,
                fontFamily: "Montserrat",
                color: "#fff",
                textAlign: "center",
              }}
            >
              Docente
            </Text>
          </TouchableOpacity>
        </View>
        <StatusBar style="auto" />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#191A1A",
    alignItems: "center",
    justifyContent: "center",
  },

  containBox: {
    flex: 0,
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: "#191A1A",
    borderRadius: 30,
    width: 350,
    height: 600,
  },

  btnRol: {
    flex: 0,
    justifyContent: "center",
    borderWidth: 2,
    borderColor: "#2FEFD5",
    width: 170,
    height: 170,
    marginBottom: 30,
    borderRadius: 30,
  },
  containTitle: {
    flex: 1,
    alignItems: "center",
    paddingTop: 30,
  },
});
